#!/bin/sh

set -e
set -x

script_dir="$( cd $(dirname $0); pwd )"

# Install the tools we'll need if we're running in Gitlab CI
if [ "$CI_PROJECT_PATH" = "vtk/vtk-wasm-docker" ]; then
    dnf install -y podman-docker crun
else
    echo "Assuming you've installed podman and crun"
fi

# Push the images to DockerHub.
readonly image_dated_tag="$( cat $script_dir/TAG )"
readonly image_latest_tag="latest"

# Use podman to avoid having to do docker-in-docker shenanigans.
readonly docker="podman --storage-driver=vfs"

$docker image load -i $script_dir/vtk-wasm-emscripten.tar
$docker tag "kitware/vtk-wasm:$image_dated_tag" "kitware/vtk-wasm:$image_latest_tag"

set +x
echo "Logging into index.docker.io ..."
$docker login --username "$DOCKERHUB_USERNAME" --password "$DOCKERHUB_PASSWORD" "index.docker.io"
set -x
$docker push "kitware/vtk-wasm:$image_dated_tag"
$docker push "kitware/vtk-wasm:$image_latest_tag"
