#!/bin/sh

set -e
set -x

current_dir="$( pwd )"
script_dir="$( cd $(dirname $0); pwd )"

# Pick up a VTK tag/branch/commit to checkout
# TODO: set VTK_VCS_REF externally with vX.Y.Z for VTK releases.
if [[ -z "$VTK_VCS_REF" ]]; then
    vtk_vcs_ref="master";
else
    vtk_vcs_ref=$VTK_VCS_REF
fi

readonly vtk_vcs_ref
readonly vtk_vcs_url="https://gitlab.kitware.com/vtk/vtk.git"

# Install the tools we'll need if we're running in Gitlab CI
if [ "$CI_PROJECT_PATH" = "vtk/vtk-wasm-docker" ]; then
    dnf install -y git-core podman-docker crun
else
    echo "Assuming you've installed git, podman and crun"
fi

# Clone VTK
[ -d "$script_dir/.vtk" ] || git clone $vtk_vcs_url $script_dir/.vtk
cd $script_dir/.vtk
git checkout $vtk_vcs_ref
git submodule update --init --recursive
# Acquire recent release tag and number of commits ahead of that tag.
readonly vtk_version="$(git describe)"
# Setup sccache.
.gitlab/ci/sccache.sh
cd $current_dir

# Use podman to avoid having to do docker-in-docker shenanigans.
readonly docker="podman --storage-driver=vfs"

# Construct image tags from vtk_version and date.
# Ex: kitware/vtk-wasm:vX.Y.Z-<commits_ahead>-g<abcdefghij>-YYYYMMDD <- Built with VTK master/other branch or a specific commit.
#     kitware/vtk-wasm:vX.Y.Z-YYYYMMDD <- Built with VTK vX.Y.Z
#     kitware/vtk-wasm:latest <- Always points to most recent image.
readonly image_dated_tag="$vtk_version-$( date "+%Y%m%d" )"

# # Build the new image.
$docker build --format=docker \
    --cgroup-manager=cgroupfs \
    --volume "$script_dir/.vtk:/VTK-src:Z" \
    "--build-arg=BUILD_DATE=$(date)" \
    "--build-arg=SCCACHE_REDIS=$SCCACHE_REDIS" \
    "--build-arg=IMAGE_TAG=$image_dated_tag" \
    "--build-arg=REVISION=$vtk_vcs_ref" \
    "--build-arg=SOURCE_URL=$vtk_vcs_url" \
    "--build-arg=VERSION=$vtk_version" \
    -t "kitware/vtk-wasm:$image_dated_tag" \
    $script_dir $@ \
    2>&1 | tee $script_dir/build.log

$docker save --format=docker-archive -o "$script_dir/vtk-wasm-emscripten.tar" "kitware/vtk-wasm:$image_dated_tag"
echo "$image_dated_tag" > "$script_dir/TAG"
