# Building vtk-wasm image on a local machine

Building the image as the CI does can be done by following these steps:

```sh
# Build the docker image
./.gitlab/ci/docker/build.sh
```
