# VTK-WebAssembly Docker Image

This repository hosts the sources to build and publish [kitware/vtk-wasm](https://hub.docker.com/r/kitware/vtk-wasm) image. 

[kitware/vtk-wasm](https://hub.docker.com/r/kitware/vtk-wasm) provides VTK static libraries built with [emscripten](https://emscripten.org). With the help of [kitware/vtk-wasm](https://hub.docker.com/r/kitware/vtk-wasm) and tooling borrowed from [itk-wasm](https://wasm.itk.org), it is very easy to integrate VTK-wasm into your web workflow for development and production.

## Usage

### Basic
Here is a guide that walks through compiling a pure VTK-C++ WebAssembly application with `itk-wasm` CLI and `kitware/vtk-wasm` docker image.

1. First, let's download a C++ example from VTK repository. The source code for this application can be read
[here](https://gitlab.kitware.com/vtk/vtk/-/blob/master/Examples/Emscripten/Cxx/Cone/Cone.cxx)
    ```bash
    $ curl -L --output Cone.zip https://gitlab.kitware.com/vtk/vtk/-/archive/master/vtk-master.zip?path=Examples/Emscripten/Cxx/Cone
    $ unzip Cone.zip
    $ cd vtk-master-Examples-Emscripten-Cxx-Cone/Examples/Emscripten/Cxx/Cone
    ```

2. Configure and build the application with `itk-wasm` CLI. The `itk-wasm` CLI is shipped with the `itk-wasm` Node.js package.
Install `itk-wasm` with the Node Package Manager, `npm`, the CLI shipped with Node.js.
    ```bash
    $ npm init --yes
    $ npm install itk-wasm@1.0.0-b.83
    ```

3. Build the project with the emscripten toolchain from the `kitware/vtk-wasm` image in the `./build-emscripten` directory:
    ```bash
    $ npx itk-wasm -i kitware/vtk-wasm -b ./build-emscripten -s . build
    ```

    A `Cone.wasm` WebAssembly binary is built in the `./build-emscripten` directory. Also, you'll find JavaScript glue
    `Cone.js` and the webpage `index.html`, both of which load and run `Cone.wasm` in a browser.

    **Tip**: In order to get readable stack traces or step through VTK-C++ in the browser/wasm runtime, just link to VTK debug libraries with a `-- Debug` argument at the end, like so:
    ```bash
    $ npx itk-wasm -i kitware/vtk-wasm -b ./build-emscripten -s . build -- Debug
    ```

4. Run in the browser
    ```bash
    $ python3 -m http.server -d build-emscripten 8080
    ```
Open http://localhost:8080

### Advanced (webpack integration)

In this guide, let's go through steps to integrate VTK-wasm with WebPack and npm scripts. Here's what the directory structure can look like:

```bash
src
|-App.cpp # C++ code for VTK-wasm application
|-App.h # C++ code for VTK-wasm application (does not expose VTK objects)
|-main.cpp # Optional, no wasm, runs the application natively.
|-CMakeLists.txt # Logic to build App, knows nothing about web/
web
|-index.html
|-index.js # Initialize wasm module, loads App from C++ land, does web stuff
package.json # Metadata and script hooks to build the project for web.
webpack.config.js # Logic to build and serve webpage
```

First, install [itk-wasm](https://wasm.itk.org) and save it in `package.json`
```
$ npm install --save-dev itk-wasm@1.0.0-b.83
```

Now, let's teach `package.json` the way to build our C++ code with [kitware/vtk-wasm](https://hub.docker.com/r/kitware/vtk-wasm) and [itk-wasm](https://wasm.itk.org) CLI.

```json
"name": ...
"version": ...
"description": ...
"main": "index.js"
"scripts": {
    "build-wasm": "npm run build-wasm:release",
    "build-wasm:release": "npx itk-wasm -i kitware/vtk-wasm -b build-emscripten -s src build -- Release -DDEBUGINFO=NONE",
    "build-wasm:profile": "npx itk-wasm -i kitware/vtk-wasm -b build-emscripten -s src build -- Release -DDEBUGINFO=PROFILE",
    "build-wasm:debug": "npx itk-wasm -i kitware/vtk-wasm -b build-emscripten -s src build -- Debug -DDEBUGINFO=DEBUG_NATIVE -DOPTIMIZE=NO_OPTIMIZATION",
}
```

After the C++ code is built, the `*.wasm` and `*.js` glue code sit in `src/build-emscripten`. Let's teach `webpack.config.js` to bundle and serve these files along with contents from `web/`.

```js
plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: path.join(
            __dirname, "src", "build-emscripten", "NameOfYourWasmApp.js"
          ),
          to: path.join(__dirname, "dist", "NameOfYourWasmApp.js")
        },
        {
          from: path.join(
            __dirname, "src", "build-emscripten", "NameOfYourWasmApp.wasm"
          ),
          to: path.join(__dirname, "dist", "NameOfYourWasmApp.wasm")
        }
      ],
    })
]
```

[vtkWasmBenchmark](https://github.com/jspanchu/vtkWasmBenchmark) is a complete example of webpack integration with [kitware/vtk-wasm](https://hub.docker.com/r/kitware/vtk-wasm) and [itk-wasm](https://wasm.itk.org) CLI.
